FROM python:3.8.5-slim-buster

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:/opt/feature_extractor_temporal/src/"

VOLUME [/storage]

COPY src /opt/feature_extractor_temporal/src/
ENTRYPOINT [ "python", "/opt/feature_extractor_temporal/src/main.py" ]
