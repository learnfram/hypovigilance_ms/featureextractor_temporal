#!/usr/bin/env python3

import logging
import uuid

import numpy as np
import pandas as pd
from learnfram.application.Application import Application
from learnfram.services.RawDataComponent import RawDataComponent


class Service(RawDataComponent):

    component_type = "feature_extractor_temporal"

    def run(self):

        features_extracted = self.threaded_extraction()

        logging.info(f"{len(features_extracted)} feature(s) extracted")

        file_name = "temporal_features_{}.csv".format(uuid.uuid1())

        logging.info(f"The file {file_name} will be created")

        print(features_extracted)

        self.send(file_name, features_extracted)

    def threaded_extraction(self):

        feature_extracted = {
            "min": [],
            "max": [],
            "mean": [],
            "var": [],
        }

        df = pd.DataFrame.from_dict(self.data)

        # TODO Find a better way
        for i in range(len(df)):
            l = []
            for column in df:
                l.extend(df.iloc[i][column])
            feature_extracted["min"].append(float(np.min(l)))
            feature_extracted["max"].append(float(np.max(l)))
            feature_extracted["mean"].append(float(np.mean(l)))
            feature_extracted["var"].append(float(np.var(l)))

        return feature_extracted


if __name__ == "__main__":
    Application.start(Service)
